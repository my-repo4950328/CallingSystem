using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using WebRTC.Controllers;
using WebRTC.Helpers;
using WebRTC.Hubs;
using WebRTC.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages().AddRazorRuntimeCompilation();
builder.Services.AddSignalR();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHttpContextAccessor();
builder.Services.AddSession(s => s.IdleTimeout = TimeSpan.FromMinutes(30));
builder.Services.AddAutoMapper(typeof(Program));
//builder.Services.AddRazorPages();
builder.Services.AddDbContext<DataContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("WebRTCDatabase"));
});
builder.Services.AddScoped<IVisitorsAppService,VisitorsAppService>();
//builder.Services.AddSingleton<List<Visitor>>();
builder.Services.AddSingleton<List<UserCall>>();
//builder.Services.AddSingleton<List<CallOffer>>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    //app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options => {
        options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
    });
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseSession();
app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();
app.MapHub<ChatHub>("/chatHub");
app.MapGet("/api/visitors/logout/{id:Guid}", (Guid id, IVisitorsAppService service) =>
{
    service.LogOutVisitor(id);
});
app.MapGet("/api/visitors/login/{id:Guid}", (Guid id, IVisitorsAppService service) =>
{
    service.LogIVisitor(id);
});
app.Run();
