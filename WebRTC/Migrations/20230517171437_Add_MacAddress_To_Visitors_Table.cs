﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebRTC.Migrations
{
    /// <inheritdoc />
    public partial class Add_MacAddress_To_Visitors_Table : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MacAddress",
                table: "Visitors",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MacAddress",
                table: "Visitors");
        }
    }
}
