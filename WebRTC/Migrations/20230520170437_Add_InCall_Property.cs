﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebRTC.Migrations
{
    /// <inheritdoc />
    public partial class Add_InCall_Property : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "InCall",
                table: "Visitors",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InCall",
                table: "Visitors");
        }
    }
}
