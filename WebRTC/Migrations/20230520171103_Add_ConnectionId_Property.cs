﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebRTC.Migrations
{
    /// <inheritdoc />
    public partial class Add_ConnectionId_Property : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ConnectionId",
                table: "Visitors",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConnectionId",
                table: "Visitors");
        }
    }
}
