﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebRTC.Migrations
{
    /// <inheritdoc />
    public partial class Add_CallOffers : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CallOffers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CallerId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CalleeId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CallerConnectionId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CallerSessionId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CalleeConnectionId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CalleeSessionId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CallOffers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CallOffers_Visitors_CalleeId",
                        column: x => x.CalleeId,
                        principalTable: "Visitors",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CallOffers_Visitors_CallerId",
                        column: x => x.CallerId,
                        principalTable: "Visitors",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_CallOffers_CalleeId",
                table: "CallOffers",
                column: "CalleeId");

            migrationBuilder.CreateIndex(
                name: "IX_CallOffers_CallerId",
                table: "CallOffers",
                column: "CallerId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CallOffers");
        }
    }
}
