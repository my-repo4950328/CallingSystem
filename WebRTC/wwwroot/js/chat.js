﻿$(function () {
    "use strict";
    $(".micDiv").on('click', function (e) {
        let className = $('.micLine').attr('class');

        if (localStream != null) {
            let audioTracks = localStream.getAudioTracks();

            if (audioTracks.length > 0) {

                if (className.indexOf('hidden') == -1) {
                    audioTracks[0].enabled = true;
                    $(".micLine").addClass('hidden');
                    wsconn.invoke('unMute');
                }

                else {
                    audioTracks[0].enabled = false;
                    $(".micLine").removeClass('hidden');
                    wsconn.invoke('mute');
                }
            }
        } else {
            if (className.indexOf('hidden') == -1) {
                $(".micLine").addClass('hidden');
                wsconn.invoke('unMute');
            }

            else {
                $(".micLine").removeClass('hidden');
                wsconn.invoke('mute');
            }
        }


            
    })

    //document.onvisibilitychange = () => {
    //    event.preventDefault();
    //    var a = mobileAndTabletCheck();
    //    
    //    if (a == true) {
    //        if (document.visibilityState === "hidden") {
    //            var id = $("#VisitorId").val();
    //            $.ajax({

    //                url: `/api/visitors/logout/${id}`,
    //                type: 'GET',
    //            }).done(function () {
    //                wsconn.invoke('updateUsers');
    //            });
    //            return event.returnValue = '';
    //        }
    //    }

    //};
    //window.addEventListener("beforeunload", (event) => {
    //    event.preventDefault();
    //    var a = mobileAndTabletCheck();
    //    
    //    if (a == false || a == true) {
    //        var id = $("#VisitorId").val();
    //        $.ajax({

    //            url: `/api/visitors/logout/${id}`,
    //            type: 'GET',
    //        }).done(function () {
    //            wsconn.invoke('updateUsers');
    //        });
    //        return event.returnValue = '';
    //    }
    //});

    //function mobileAndTabletCheck () {
    //    let check = false;
    //    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    //    return check;
    //};
    //window.onbeforeunload = function () {
    //    var id = $("#VisitorId").val();
    //    $.ajax({

    //        url: `/api/visitors/logout/${id}`,
    //        type: 'GET',
    //    }).done(function () {
    //        wsconn.invoke('updateUsers');
    //    });
        
    //}
    $(document).ready(function ()
    {
        hideStatus();
        $(".otherSideMuted").hide();
        var x = document.getElementById("Audio");
        x.muted = false;
       
        //var id = $("#VisitorId").val();
        //$.ajax({
        //    url: `/api/visitors/login/${id}`,
        //    type: 'GET',
        //    success: function () {

        //    },
        //    error: function (request, error) {
        //    }
        //});
        initializeSignalR();
    })
    
    var hubUrl = document.location.pathname + 'chatHub';
    var wsconn = new signalR.HubConnectionBuilder()
        .withUrl(hubUrl, signalR.HttpTransportType.WebSockets)
        .configureLogging(signalR.LogLevel.None).build();
    /*var peerConnectionConfig = { "iceServers": [{ "url": "stun:stun.l.google.com:19302" }],"iceCandidatePoolSize ":1};*/
    //var peerConnectionConfig = { "iceServers": [{ "url": "stun:stun.l.google.com:19302" }]};
    /*var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();*/
    var peerConnectionConfig = {};

    (async () => {
        const response = await fetch(
            "https://cloudsystems.metered.live/api/v1/turn/credentials?apiKey=ed9f0cbea30c5873faecef524daa754a149f"
        );
        var iceServers = [];
        const tempiceServers = await response.json();
        if (tempiceServers.length > 0) {
            for (var i = 0; i < tempiceServers.length; i++) {
                var server = tempiceServers[i]['urls'];
                let result = server.includes("stun");
                if (!result) {
                    iceServers.push(tempiceServers[i]);
                }
            }
        }
        
        peerConnectionConfig.iceServers = iceServers;
    })();
    //let peerConnectionConfig = {
    //    iceServers: [
    //        {
    //            urls: ["stun:stun1.1.google.com:19302", "stun:stun2.1.google.com:19302"],
    //        },
    //    ],
    //};
    var webrtcConstraints = { audio: true, video: false };
    
    var WOWZA_STREAM_NAME = null, connections = {}, localStream = null;

    const attachMediaStream = (e) => {
        
        //console.log(e);
        console.log("OnPage: called attachMediaStream");
        var partnerAudio = document.querySelector('.audio.partner');
        if (partnerAudio.srcObject !== e.stream) {
            
            partnerAudio.srcObject = e.stream;
            console.log("OnPage: Attached remote stream");
        }
    };

    //Disable the send button until connection is established.
    /*document.getElementById("sendButton").disabled = true;*/

    //connection.on("ReceiveMessage", function (user, message) {
    //    var li = document.createElement("li");
    //    document.getElementById("messagesList").appendChild(li);
    //    // We can assign user-supplied strings to an element's textContent because it
    //    // is not interpreted as markup. If you're assigning in any other way, you 
    //    // should be aware of possible script injection concerns.
    //    li.textContent = `${user} says ${message}`;
    //});

    //connection.start().then(function () {
    //    document.getElementById("sendButton").disabled = false;
    //}).catch(function (err) {
    //    return console.error(err.toString());
    //});

    //document.getElementById("sendButton").addEventListener("click", function (event) {
    //    var user = document.getElementById("userInput").value;
    //    var message = document.getElementById("messageInput").value;
    //    wsconn.invoke("SendMessage", user, message).catch(function (err) {
    //        return console.error(err.toString());
    //    });
    //    event.preventDefault();
    //});


    const initializeSignalR = () => {
        
        wsconn.start().then(() => { console.log("SignalR: Connected"); askUsername(); }).catch(err => console.log(err));
    };
    const askUsername = () => {
        
        generateRandomUsername();
    };
    const generateRandomUsername = () => {
        var visitorId = $("#VisitorId").val();
        
        setUsername(visitorId);
    };

    function setUsername(username) {
        //initializeUserMedia();
    }
    function stopMicUse() {
        
        localStream.getTracks().forEach(track => {
            track.stop();
        });
    }
    const callbackUserMediaSuccess = (stream) => {
        
        console.log("WebRTC: got media stream");
        localStream = stream;

        const audioTracks = localStream.getAudioTracks();
        if (audioTracks.length > 0) {
            let className = $('.micLine').attr('class');
            if (className.indexOf('hidden') == -1)
                audioTracks[0].enabled = false;
            else
                audioTracks[0].enabled = true;
            if (AllowMic) {

                $('#rotateDiv').addClass('rotate');
                $(".Elipse3").removeClass('beat');
                wsconn.invoke('callUser');
            }
            else {
                let error = new DOMException("Permission denied", "NotAllowedError")
                openErrorModal(error);

            }
            console.log(`Using Audio device: ${audioTracks[0].label}`);
        }
    };

    //function handleVisibilityChange() {
    //    if (document.hidden) {

    //    } else {

    //    }
    //}

    //document.addEventListener("visibilitychange", handleVisibilityChange, false);

    const initializeUserMedia = () => {
        var a = navigator;
        
        console.log('WebRTC: InitializeUserMedia: '); 
        navigator.getUserMedia(webrtcConstraints, callbackUserMediaSuccess, errorHandler);
        
    };

    const callbackRemoveStream = (connection, evt) => {
        console.log('WebRTC: removing remote stream from partner window');
        // Clear out the partner window
        var otherAudio = document.querySelector('.audio.partner');
        otherAudio.src = '';
    }

    const callbackAddStream = (connection, evt) => {
        console.log('WebRTC: called callbackAddStream');

        // Bind the remote stream to the partner window
        //var otherVideo = document.querySelector('.video.partner');
        //attachMediaStream(otherVideo, evt.stream); // from adapter.js
        attachMediaStream(evt);
    }

    const callbackNegotiationNeeded = (connection, evt) => {
        
        console.log("WebRTC: Negotiation needed...");
        //console.log("Event: ", evt);
    }

    const callbackIceCandidate = (evt, connection, partnerClientId) => {
        console.log("WebRTC: Ice Candidate callback");
        //console.log("evt.candidate: ", evt.candidate);
        if (evt.candidate) {// Found a new candidate
            
            console.log('WebRTC: new ICE candidate');
            //console.log("evt.candidate: ", evt.candidate);
            sendHubSignal(JSON.stringify({ "candidate": evt.candidate }), partnerClientId);
        } else {
            // Null candidate means we are done collecting candidates.
            console.log('WebRTC: ICE candidate gathering complete');
            sendHubSignal(JSON.stringify({ "candidate": null }), partnerClientId);
        }
    }
    function hideStatus() {
        $(".connectionStatus").hide();
        $(".circle").addClass('hidden');
    }

    function showConnecting() {
        $("#ConnectingDot").removeClass("greenDot3");
        $("#ConnectingDot").addClass("greenDot2");
        $(".ConnectingText").html('<p style="font-size:14px!important">Connecting</p><div class="loading"><span class="rounded-circle loading__dot"></span><span class="rounded-circle loading__dot"></span><span class="rounded-circle loading__dot"></span></div > ');
      $(".connectionStatus").show();
        $(".circle").removeClass("hidden");
        
    }
    function showDisConnected() {
        $("#ConnectingDot").removeClass("greenDot2");
        $("#ConnectingDot").addClass("greenDot3");
        $(".ConnectingText").html('Disconnected');
        $(".connectionStatus").show();
        $(".circle").removeClass("hidden");

    }
    function showConnected() {
        $("#ConnectingDot").removeClass("greenDot3");
        $("#ConnectingDot").addClass("greenDot2");
        $(".ConnectingText").html('&nbsp;&nbsp;Connected');
        $(".connectionStatus").show();
        
    }
    $("#callIcon").on('click', function () {
        startProcess();
    })
    function startProcess() {
        var classes = $(".circle").attr('class');
        if (classes != null && classes != undefined && classes.indexOf("hidden") >= 0) {
            initializeUserMedia();



        }

        else {
            stopMicUse();
            hideStatus();
            $('#rotateDiv').removeClass('rotate');
            $(".Elipse3").addClass('beat')
            wsconn.invoke('makeInvisable');
        }
    }

    wsconn.on('OtherSideMute', () => {
        $(".otherSideMuted").show();
        $(".connectionStatus").hide();
    })
    wsconn.on('OtherSideUnMute', () => {
        $(".otherSideMuted").hide();
        $(".connectionStatus").show();
    })
    wsconn.on('updateUserList', (userList) => {
        $(".headerContainer").html('');
        $(".headerContainer").html(`${userList.length}`);
    })

    wsconn.on('noUsersToCall', (reason) => {
        // Let the user know that the callee declined to talk
        alertify.error(reason);
    });

    wsconn.on('Agree', () => {
        showConnecting();
    })

    wsconn.on('Reject', () => {
        $(".index").removeClass('blured');
        $('#agreement-modal').animate({
            'top': '0px'
        }, { "duration": 1000, "queue": false });
        $('#agreement-modal').fadeOut({ "duration": 1000, "queue": false });
        var msg = alertify.message('Default message');
        msg.delay(2).setContent('please accept our agreement in order to start calling');
        //alertify.error('please accept our agreement in order to start calling');
    })
    $('#agreement-modal #acceptBtn').on('click', function (e) {
        wsconn.invoke('acceptAgreement');
        $(".index").removeClass('blured');
        $('#agreement-modal').animate({
            'top': '0px'
        }, { "duration": 1000, "queue": false });
        $('#agreement-modal').fadeOut({ "duration": 1000, "queue": false });

    })
    $('#agreement-modal #rejectBtn').on('click', function (e) {
        wsconn.invoke('rejectAgreement');
    })
    wsconn.on('UserAgreement', (callingUser) => {
        $(".index").addClass('blured');
        $('#agreement-modal').animate({
            'top': '343px'
        }, { "duration": 1000, "queue": false });
        $('#agreement-modal').fadeIn({ "duration": 1000, "queue": false });
        /*window.location.href="/Agreement"*/
    })

    wsconn.on('incomingCall', (callingUser) => {
        wsconn.invoke('AnswerCall', true, callingUser).catch(err => console.log(err));
        //document.getElementById("Audio").play();
        //console.log('SignalR: incoming call from: ' + JSON.stringify(callingUser));
        //Swal.fire({
        //    title: '<strong>Incoming call</strong>',
        //    html:`a user trying to call you.Do you want to chat?`,
        //    showCloseButton: false,
        //    showCancelButton: true,
        //    focusConfirm: false,
        //    allowOutsideClick: false,
        //    confirmButtonText:
        //        '<i class="fa fa-phone"></i> Accept!',
        //    cancelButtonText:
        //        '<i class="fa fa-phone"></i> Decline!'
        //}).then(function (result) {
        //    if (result.value) {
        //        document.getElementById("Audio").pause();
        //        document.getElementById("Audio").currentTime = 0;
        //        wsconn.invoke('AnswerCall', true, callingUser).catch(err => console.log(err));
        //    } else if (result.dismiss == 'cancel') {
        //        document.getElementById("Audio").pause(); document.getElementById("Audio").currentTime = 0; wsconn.invoke('AnswerCall', false, callingUser).catch(err => console.log(err)); 
        //    }

        //});
    });

    wsconn.on('callEnded', (signalingUser, signal) => {
        //console.log(signalingUser);
        //console.log(signal);

        console.log('SignalR: call with ' + signalingUser.connectionId + ' has ended: ' + signal);

        // Let the user know why the server says the call is over
        /*alertify.error(signal);*/
        var msg = alertify.message('Default message');
        msg.delay(3).setContent('<img class="hang-up" src="/img/hang-up.png" />' + signal);

        // Close the WebRTC connection
        closeConnection(signalingUser.connectionId);
        
        $("#callIcon").show();
        $("#declineIcon").hide();
        hideStatus();
        $('.globe').removeClass('hidden');
        $('.countryFlag').addClass('hidden');
        $(".connectionStatus").show();
        $(".otherSideMuted").hide();
        showDisConnected();
        setTimeout(function () {
            hideStatus();
            $('#rotateDiv').removeClass('rotate');
            $(".Elipse3").addClass('beat')
            wsconn.invoke('makeInvisable');
            stopMicUse();
        }, 2000);
        //$("#HangUpIconDiv").css('display', 'none');
        //$("#HangUpIconDiv .DefaultFlag").hide();
        //$("#HangUpIconDiv .Flag").remove();
    });

    // Close the connection between myself and the given partner
    const closeConnection = (partnerClientId) => {
        console.log("WebRTC: called closeConnection ");
        var connection = connections[partnerClientId];

        if (connection) {
            // Let the user know which streams are leaving
            // todo: foreach connection.remoteStreams -> onStreamRemoved(stream.id)
            onStreamRemoved(null, null);

            // Close the connection
            connection.close();
            delete connections[partnerClientId]; // Remove the property
        }
    }

    const closeAllConnections = () => {
        console.log("WebRTC: call closeAllConnections ");
        for (var connectionId in connections) {
            closeConnection(connectionId);
        }
    }

    const onStreamRemoved = (connection, streamId) => {
        console.log("WebRTC: onStreamRemoved -> Removing stream: ");
        //console.log("Stream: ", streamId);
        //console.log("connection: ", connection);
    }

    wsconn.on('callDeclined', (decliningUser, reason) => {
        hideStatus();
        $('.globe').removeClass('hidden');
        $('#rotateDiv').removeClass('rotate');
        $('.countryFlag').addClass('hidden');
        console.log('SignalR: call declined from: ' + decliningUser.connectionId);

        // Let the user know that the callee declined to talk
        alertify.error(reason);
    });

    wsconn.on('callAccepted', (acceptingUser) => {
        console.log('SignalR: call accepted from: ' + JSON.stringify(acceptingUser) + '.  Initiating WebRTC call and offering my stream up...');
        // Callee accepted our call, let's send them an offer with our video stream
        initiateOffer(acceptingUser.connectionId, localStream); // Will use driver email in production
    });


    const initiateOffer = (partnerClientId, stream) => {
        console.log('WebRTC: called initiateoffer: ');
        var connection = getConnection(partnerClientId); // // get a connection for the given partner
        //console.log('initiate Offer stream: ', stream);
        //console.log("offer connection: ", connection);
        connection.addStream(stream);// add our audio/video stream
        console.log("WebRTC: Added local stream");

        connection.createOffer().then(offer => {
            
            console.log('WebRTC: created Offer: ');
            console.log('WebRTC: Description after offer: ', offer);
            connection.setLocalDescription(offer).then(() => {
                console.log('WebRTC: set Local Description: ');
                console.log('connection before sending offer ', connection);
                sendHubSignal(JSON.stringify({ "sdp": connection.localDescription }), partnerClientId);
            }).catch(err => console.error('WebRTC: Error while setting local description', err));
        }).catch(err => console.error('WebRTC: Error while creating offer', err));

        //connection.createOffer((desc) => { // send an offer for a connection
        //    console.log('WebRTC: created Offer: ');
        //    console.log('WebRTC: Description after offer: ', JSON.stringify(desc));
        //    connection.setLocalDescription(desc, () => {
        //        console.log('WebRTC: Description after setting locally: ', JSON.stringify(desc));
        //        console.log('WebRTC: set Local Description: ');
        //        console.log('connection.localDescription: ', JSON.stringify(connection.localDescription));
        //        sendHubSignal(JSON.stringify({ "sdp": connection.localDescription }), partnerClientId);
        //    });
        //}, errorHandler);
    }

    const getConnection = (partnerClientId) => {
        console.log("WebRTC: called getConnection");
        if (connections[partnerClientId]) {
            console.log("WebRTC: connections partner client exist");
            return connections[partnerClientId];
        }
        else {
            console.log("WebRTC: initialize new connection");
            return initializeConnection(partnerClientId)
        }
    }

    const sendHubSignal = (candidate, partnerClientId) => {
        
        console.log('candidate', candidate);
        console.log('SignalR: called sendhubsignal ');
        wsconn.invoke('sendSignal', candidate, partnerClientId).catch(errorHandler);
    };

    const initializeConnection = (partnerClientId) => {

        console.log('WebRTC: Initializing connection...');
        //console.log("Received Param for connection: ", partnerClientId);

        var connection = new RTCPeerConnection(peerConnectionConfig);

        //connection.iceConnectionState = evt => console.log("WebRTC: iceConnectionState", evt); //not triggering on edge
        //connection.iceGatheringState = evt => console.log("WebRTC: iceGatheringState", evt); //not triggering on edge
        //connection.ondatachannel = evt => console.log("WebRTC: ondatachannel", evt); //not triggering on edge
        //connection.oniceconnectionstatechange = evt => console.log("WebRTC: oniceconnectionstatechange", evt); //triggering on state change 
        //connection.onicegatheringstatechange = evt => console.log("WebRTC: onicegatheringstatechange", evt); //triggering on state change 
        //connection.onsignalingstatechange = evt => console.log("WebRTC: onsignalingstatechange", evt); //triggering on state change 
        //connection.ontrack = evt => console.log("WebRTC: ontrack", evt);
        connection.onicecandidate = evt => { callbackIceCandidate(evt, connection, partnerClientId) };
        // ICE Candidate Callback
        //connection.onnegotiationneeded = evt => callbackNegotiationNeeded(connection, evt); // Negotiation Needed Callback
        connection.onaddstream = evt => { callbackAddStream(connection, evt); } // Add stream handler callback
        connection.onremovestream = evt => callbackRemoveStream(connection, evt);
        //connection.onnegotiationneeded = evt => { }; // Remove stream handler callback
        connections[partnerClientId] = connection;
        // Store away the connection based on username
        //console.log(connection);
        return connection;
    }

    wsconn.on('receiveSignal', (signalingUser, signal, flag) => {
        //console.log('WebRTC: receive signal ');
        //console.log(signalingUser);
        //console.log('NewSignal', signal);

        newSignal(signalingUser.connectionId, signal, flag);
    });

    const newSignal = (partnerClientId, data,flag) => {
        
        console.log('WebRTC: called newSignal');
        //console.log('connections: ', connections);

        var signal = JSON.parse(data);
        var connection = getConnection(partnerClientId);
        //console.log("signal: ", signal);
        //console.log("signal: ", signal.sdp || signal.candidate);
        //console.log("partnerClientId: ", partnerClientId);
        console.log("connection: ", connection);

        // Route signal based on type
        if (signal.sdp) {
            console.log('WebRTC: sdp signal');
            receivedSdpSignal(connection, partnerClientId, signal.sdp);
        } else if (signal.candidate) {
            console.log('WebRTC: candidate signal');
            receivedCandidateSignal(connection, partnerClientId, signal.candidate);
        } else {
            console.log('WebRTC: adding null candidate');
            connection.addIceCandidate(null, () => console.log("WebRTC: added null candidate successfully"), () => console.log("WebRTC: cannot add null candidate"));
        }
        
        $("#callIcon").hide();
        $("#declineIcon").show();
        showConnected();
        $("#HangUpIconDiv").css('display', 'inline-block');

        let className = $('.micLine').attr('class');
        if (className.indexOf('hidden') == -1) {
            wsconn.invoke('Mute');
        }

        else {
            wsconn.invoke('unMute');
        }
        //var finded = $("#HangUpIconDiv").find(".Flag");
        //if (finded.length == 0) {

        //    if (flag.length > 0) {
        //        $("#HangUpIconDiv").append(`<img id="Flag" class="Flag" src="${flag}" style="width:50px;height:50px;cursor:pointer;display:inline-block!important;" />`);
        //    }
        //    else {
        //        $("#HangUpIconDiv .DefaultFlag").show();
        //    }

        //}
        //$('.globe').addClass('hidden');
        //$('.countryFlag').css('background-image', `url(${flag})`)
        //$('.countryFlag').removeClass('hidden');
        if (flag != '') {
            $('.globe').addClass('hidden');
            $('.countryFlag').css('background-image', `url(${flag})`)
            $('.countryFlag').removeClass('hidden');
        }
        else {
            $('.globe').removeClass('hidden');
            ('#rotateDiv').addClass('rotate');
            $('.countryFlag').addClass('hidden');
        }
        //if (flag != '')
        //    $("#flag").attr('src', `${flag}`);
        //else
        //    $("#flag").attr('src', `/img/glob.png`);
    }

    //const onReadyForStream = (connection) => {
    //    console.log("WebRTC: called onReadyForStream");
    //    // The connection manager needs our stream
    //    //console.log("onReadyForStream connection: ", connection);
    //    connection.addStream(localStream);
    //    console.log("WebRTC: added stream");
    //}

    const receivedCandidateSignal = (connection, partnerClientId, candidate) => {
        //console.log('candidate', candidate);
        //if (candidate) {
        console.log('WebRTC: adding full candidate');
        connection.addIceCandidate(new RTCIceCandidate(candidate), () => console.log("WebRTC: added candidate successfully"), () => console.log("WebRTC: cannot add candidate"));
        //} else {
        //    console.log('WebRTC: adding null candidate');
        //   connection.addIceCandidate(null, () => console.log("WebRTC: added null candidate successfully"), () => console.log("WebRTC: cannot add null candidate"));
        //}
    }
    $('#declineIcon').click(function () {
        showByeMessage();
        setTimeout(function () {
            hideByeMessage();
        }, 4000);
        


        //console.log('hangup....');
        //wsconn.invoke('hangUp');
        //closeAllConnections();
    });


    $(document).on('click','#ByeDiv', function (e) {
        console.log('hangup....');
        wsconn.invoke('hangUp');
        closeAllConnections();
    })

    function showByeMessage() {
        $("#endCallDiv").html('');
        $("#endCallDiv").append('<div id="ByeDiv"><div id="end">end</div></div>');
    }

    function hideByeMessage() {
        $("#endCallDiv").html('');
        $("#endCallDiv").append('<img id="endCallIcon" class="Vector" src="/img/VectorcallEnd.png" /><div class="endcircle" style = "animation-delay:0s" ></div><div class="endcircle" style="animation-delay:1s"></div><div class="endcircle" style="animation-delay:2s"></div>')
    }
    // Process a newly received SDP signal
    const receivedSdpSignal = (connection, partnerClientId, sdp) => {
        console.log('connection: ', connection);
        console.log('sdp', sdp);
        console.log('WebRTC: called receivedSdpSignal');
        console.log('WebRTC: processing sdp signal');
        connection.setRemoteDescription(new RTCSessionDescription(sdp), () => {
            console.log('WebRTC: set Remote Description');
            if (connection.remoteDescription.type == "offer") {
                console.log('WebRTC: remote Description type offer');
                connection.addStream(localStream);
                console.log('WebRTC: added stream');
                connection.createAnswer().then((desc) => {
                    console.log('WebRTC: create Answer...');
                    connection.setLocalDescription(desc, () => {
                        console.log('WebRTC: set Local Description...');
                        console.log('connection.localDescription: ', connection.localDescription);
                        //setTimeout(() => {
                        sendHubSignal(JSON.stringify({ "sdp": connection.localDescription }), partnerClientId);
                        //}, 1000);
                    }, errorHandler);
                }, errorHandler);
            } else if (connection.remoteDescription.type == "answer") {
                console.log('WebRTC: remote Description type answer');
            }
        }, errorHandler);
    }
    var AllowMic = true;
    const errorHandler = (error) => {
        
        if (error.name =='NotAllowedError')
            AllowMic = false;
        openErrorModal(error);
        //if (error.message)
        //    alertify.alert('<h4>Error Occurred</h4></br>Error Info: ' + JSON.stringify(error.message));
        //else
        //    alertify.alert('<h4>Error Occurred</h4></br>Error Info: ' + JSON.stringify(error));

        //consoleLogger(error);

    };
})
function openErrorModal(error) {
    $('.errorModalDiv').removeClass('hidden');
    $(".index").addClass('blured');
    $('#error-modal').animate({
        'top': '343px'
    }, { "duration": 1000, "queue": false });
    let msg = 'Error Info : Mic denial ';
    if (error.name == 'NotAllowedError') {
        msg += `<br/>The microphone is sad now, but you are happy. <br/>What a life.<br/>Refresh the browser before you leave`
    }
    $('#error-modal .label-content').html(msg);
    $('#error-modal').fadeIn({ "duration": 1000, "queue": false });
}
function closeErrorModal() {
    $('.errorModalDiv').addClass('hidden');
    $(".index").removeClass('blured');
    $('#error-modal').animate({
        'top': '0px'
    }, { "duration": 1000, "queue": false });
    $('#error-modal').fadeOut({ "duration": 1000, "queue": false });
}
$('#error-modal #okBtn').on('click', function (e) {

})
const consoleLogger = (val) => {
    if (isDebugging) {
        console.log(val);
    }
};
