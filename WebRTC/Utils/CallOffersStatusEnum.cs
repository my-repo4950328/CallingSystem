﻿namespace WebRTC.Utils
{
    public enum CallOffersStatusEnum
    {
        Ringing,
        Processing,
        Declined,
        Finished
        
    }
}
