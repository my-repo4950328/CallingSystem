using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebRTC.Pages
{
    public class AgreementModel : PageModel
    {
        public void OnGet()
        {
        }
        public IActionResult OnGetAgreeClick()
        {
            HttpContext.Session.SetInt32("Agreement", 1);
            return Redirect("/");
            // code goes here 
        }
        public IActionResult OnGetRejectClick()
        {
            HttpContext.Session.SetInt32("Agreement", 0);
            return Redirect("/");
            // code goes here 
        }
    }
}
