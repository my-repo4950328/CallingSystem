﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Management;
using System.Net;
using WebRTC.Controllers;

namespace WebRTC.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IVisitorsAppService _visitorsAppService;
        private readonly IHttpContextAccessor _accessor;

        public IndexModel(ILogger<IndexModel> logger, IVisitorsAppService visitorsAppService, IHttpContextAccessor accessor)
        {
            _logger = logger;
            _visitorsAppService = visitorsAppService;
            _accessor = accessor;
        }

        [BindProperty(SupportsGet =true)]
        [HiddenInput]
        public Guid VisitorId { get; set; }
        [BindProperty]
        public int VisitorCount { get; set; } = 0;

        public IActionResult OnGet()
        {
            //var agreement = HttpContext.Session.GetInt32("Agreement");
            //if (agreement == null || (agreement!=null && agreement==0)) {
            //    return Redirect("Agreement");
            //}

            //string[] filePaths = Directory.GetFiles("https://res.pokgaming.com/minigames/static/media/");
            //var a = 10;
            //var b = 100 / a;
            //string mac = String.Empty;
            //string ip = "";
            //var sessionId = _accessor.HttpContext.Session.Id;
            //var ipAdd = _accessor.HttpContext.Connection.RemoteIpAddress;
            //var port = _accessor.HttpContext.Connection.RemotePort;
            //IPHostEntry iPHostEntry =Dns.GetHostEntry(Dns.GetHostName());
            //string IpAddress = Convert.ToString(iPHostEntry.AddressList.FirstOrDefault(address=>address.AddressFamily==System.Net.Sockets.AddressFamily.InterNetwork));

            //ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            //ManagementObjectCollection moc = mc.GetInstances();

            //foreach(ManagementObject mo in moc)
            //{
            //    if(mac==String.Empty)
            //    {
            //        if ((bool)mo["IPEnabled"] == true) mac = mo["MacAddress"].ToString();
            //    }
            //    mo.Dispose();
            //}

            //mac = mac.Replace(":", "_");

            //if (HttpContext!=null && HttpContext.Connection!=null && HttpContext.Connection.RemoteIpAddress != null)
            //{
            //    ip = HttpContext.Connection.RemoteIpAddress.ToString();
            //    //mac = Request.ser.Value.ToString();
            //}
            //var findedVisitor = _visitorsAppService.GetByMacAddress(mac);
            //var sessionId = _accessor.HttpContext != null ? _accessor.HttpContext.Session.Id : Guid.NewGuid().ToString();
            //var findedVisitor = _visitorsAppService.GetBySessionId(sessionId);
            //if (findedVisitor==null)
            //    _visitorsAppService.CreateVisitor(new Models.Dto.CreateVisitorDto() { CreationTime = DateTime.Now, IsAvailable = true, LastVisitDate = DateTime.Now,SessionId=sessionId });


            //var finalFindedVisitor = _visitorsAppService.GetBySessionId(sessionId);
            //VisitorId = finalFindedVisitor!=null ? finalFindedVisitor.Id : Guid.Empty;
            //    //var Updated = _visitorsAppService.UpdateVisitor(new Models.Visitor() { Id = findedVisitor.Id, IpAddress = IpAddress, LastVisitDate = DateTime.Now, CreationTime = findedVisitor.CreationTime, IsAvailable = true, LastLogOutDate = findedVisitor.LastLogOutDate, MacAddress = findedVisitor.MacAddress });

            VisitorCount = _visitorsAppService.GetOnlineCount();
            return Page();
        }
    }
}