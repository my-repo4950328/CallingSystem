﻿using AutoMapper;
using AutoMapper.Internal.Mappers;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using WebRTC.Helpers;
using WebRTC.Hubs;
using WebRTC.Models;
using WebRTC.Models.Dto;
using WebRTC.Utils;

namespace WebRTC.Controllers
{
    public class VisitorsAppService :IVisitorsAppService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        private IHubContext<ChatHub> _hubContext { get; set; }
        public VisitorsAppService(DataContext dataContext
            , IMapper mapper
            , IHubContext<ChatHub> hubContext)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _hubContext = hubContext;
        }

        public List<Visitor> GetAll()
        {
            return _dataContext.Visitors.ToList();
        }

        public VisitorDto CreateVisitor(CreateVisitorDto createVisitorDto)
        {
            _dataContext.Visitors.Add(_mapper.Map<Visitor>(createVisitorDto));
            _dataContext.SaveChanges();
            return _mapper.Map<VisitorDto>(createVisitorDto);
        }


        public VisitorDto GetBySessionId(string sessionId)
        {
            try
            {
                VisitorDto visitorDto = new VisitorDto() { };
                var finded = _dataContext.Visitors.FirstOrDefault(a => a.SessionId != null && a.SessionId == sessionId);
                if (finded != null)
                {
                    visitorDto = _mapper.Map<VisitorDto>(finded);
                    return visitorDto;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public VisitorDto GetByMacAddress(string mac)
        {
            try
            {
                VisitorDto visitorDto = new VisitorDto() { };
                var finded = _dataContext.Visitors.FirstOrDefault(a => a.MacAddress!=null && a.MacAddress == mac);
                if (finded != null)
                {
                    visitorDto = _mapper.Map<VisitorDto>(finded);
                    return visitorDto;
                }
                else
                    return null;

            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public VisitorDto GetById(Guid id)
        {
            try
            {
                VisitorDto visitorDto = new VisitorDto() { };
                var finded = _dataContext.Visitors.FirstOrDefault(a => a.Id != id);
                if (finded != null)
                {
                    visitorDto = _mapper.Map<VisitorDto>(finded);
                    return visitorDto;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public async Task<bool> LogOutVisitor(Guid Id)
        {
            var finded = _dataContext.Visitors.FirstOrDefault(a => a.Id == Id);
            if(finded!=null)
            {
                var findedCallOffers = _dataContext.CallOffers.Where(a=>a.CallerId==finded.Id || a.CalleeId==finded.Id);
                _dataContext.CallOffers.RemoveRange(findedCallOffers);
                _dataContext.Visitors.Remove(finded);
                _dataContext.SaveChanges();
            }
            await _hubContext.Clients.All.SendAsync("updateUserList",_dataContext.Visitors.ToList());
            return true;
        }

        public async Task<bool> LogOutVisitorByConnectionId(string ConnectionId)
        {
            var finded = _dataContext.Visitors.FirstOrDefault(a => a.ConnectionId == ConnectionId);
            if (finded != null)
            {
                var findedCallOffers = _dataContext.CallOffers.Where(a => a.CallerId == finded.Id || a.CalleeId == finded.Id);
                if(findedCallOffers!=null && findedCallOffers.Count()>0)
                {
                    var findedProcessingCalls = findedCallOffers.FirstOrDefault(a => a.Status == CallOffersStatusEnum.Processing.ToString());
                    if(findedProcessingCalls!=null)
                    {
                        var findedOtherSides = _dataContext.Visitors.Where(a => a.Id == findedProcessingCalls.CallerId || a.Id == findedProcessingCalls.CalleeId);
                        if(findedOtherSides!=null && findedOtherSides.Count()>0)
                        {
                            foreach (var otherSide in findedOtherSides)
                            {
                                if(otherSide.Id!=finded.Id && otherSide.InCall==true)
                                    await _hubContext.Clients.Client(otherSide.ConnectionId).SendAsync("callEnded", otherSide, string.Format("user has hung up. maybe to mars."));
                                otherSide.InCall = false;
                            }
                            _dataContext.Visitors.UpdateRange(findedOtherSides);
                        }


                    }
                    _dataContext.CallOffers.RemoveRange(findedCallOffers);
                }
                _dataContext.Visitors.Remove(finded);
                _dataContext.SaveChanges();

            }
            await _hubContext.Clients.All.SendAsync("updateUserList", _dataContext.Visitors.ToList());
            return true;
        }

        public void LogIVisitor(Guid Id)
        {
            var finded = _dataContext.Visitors.FirstOrDefault(a => a.Id == Id);
            if (finded != null)
            {
                finded.LastVisitDate = DateTime.Now;
                finded.IsAvailable = true;
                _dataContext.Visitors.Update(finded);
                _dataContext.SaveChanges();
            }
        }

        public VisitorDto UpdateVisitor(Visitor visitor)
        {
            var finded = _dataContext.Visitors.FirstOrDefault(a=>a.Id==visitor.Id);
            if(finded != null)
            {
                finded.IpAddress = visitor.IpAddress;
                finded.IsAvailable = visitor.IsAvailable;
                finded.CreationTime = visitor.CreationTime;
                finded.LastModificationTime = visitor.LastModificationTime;
                finded.LastVisitDate = visitor.LastVisitDate;
                finded.LastLogOutDate = visitor.LastLogOutDate;
                finded.MacAddress = visitor.MacAddress;

                _dataContext.Visitors.Update(finded);
                _dataContext.SaveChanges();
            }

            return _mapper.Map<VisitorDto>(visitor);
        }

        public int GetOnlineCount()
        {
            return _dataContext.Visitors.Count(a => a.IsAvailable.HasValue && a.IsAvailable.Value);
        }

        public CallOffers AddCallOffer(Visitor Caller, Visitor Callee)
        {
            var toAdd = new CallOffers() { 
                Callee=Callee,
                Caller=Caller,
                CreationTime=DateTime.Now,
                Status = CallOffersStatusEnum.Ringing.ToString()
            };
            _dataContext.CallOffers.Add(toAdd);
            _dataContext.SaveChanges();
            return toAdd;
        }

        public List<CallOffers> GetAllCallOffers()
        {
            return _dataContext.CallOffers.Include(a=>a.Caller).Include(a=>a.Callee).ToList();
        }

        public void DeclineCallOffer(Visitor Caller, Visitor Callee)
        {
            try
            {
                CallOffers? finded = null;
                //var finded = _dataContext.CallOffers.Include(a => a.Callee).Include(a => a.Caller).First();
                var allCalls = _dataContext.CallOffers.Include(a => a.Callee).Include(a => a.Caller)
                .Where(a => a.Caller.ConnectionId == Caller.ConnectionId && a.Callee.ConnectionId == Callee.ConnectionId);
                if(allCalls!=null)
                    finded=allCalls.OrderByDescending(a => a.CreationTime).First();
                if (finded != null)
                {
                    finded.Status = CallOffersStatusEnum.Declined.ToString();
                    _dataContext.CallOffers.Update(finded);
                    _dataContext.SaveChanges();
                }
            }
            catch(Exception ex)
            {

            }

        }

        public void AcceptCallOffer(Visitor Caller, Visitor Callee)
        {
            try
            {
                var findedCaller = _dataContext.Visitors.FirstOrDefault(a=>a.Id==Caller.Id);
                var findedCalle = _dataContext.Visitors.FirstOrDefault(a => a.Id == Callee.Id);
                CallOffers? finded = null;
                //var finded = _dataContext.CallOffers.Include(a => a.Callee).Include(a => a.Caller).First();
                var allCalls = _dataContext.CallOffers.Include(a => a.Callee).Include(a => a.Caller)
                .Where(a => a.Caller.ConnectionId == Caller.ConnectionId && a.Callee.ConnectionId == Callee.ConnectionId);
                if (allCalls != null)
                    finded = allCalls.OrderByDescending(a => a.CreationTime).First();
                if (finded != null && findedCalle!=null && findedCaller!=null)
                {
                    _dataContext.UpdateRange(new List<Visitor>() { findedCalle, findedCaller });
                    finded.Status = CallOffersStatusEnum.Processing.ToString();
                    _dataContext.CallOffers.Update(finded);
                    findedCalle.InCall = true;
                    findedCaller.InCall = true;
                    _dataContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }

        }

        public void HangCall(string ConnectionId)
        {
            var findedVisitor = _dataContext.Visitors.FirstOrDefault(a => a.ConnectionId == ConnectionId);
            if(findedVisitor!=null)
            {
                findedVisitor.InCall = false;
                findedVisitor.IsAvailable = true;
                var findedCallOffer = _dataContext.CallOffers.Where(a => a.CallerId == findedVisitor.Id || a.CalleeId == findedVisitor.Id);
                if(findedCallOffer != null )
                {
                    _dataContext.CallOffers.RemoveRange(findedCallOffer);
                }
                _dataContext.SaveChanges();
            }
        }

        public string getCounryFlag(string regionName,string code)
        {
            try
            {
                string res = "";
                Country lastFinded = new Country();
                var finded = _dataContext.Countries.Where(a => a.code.ToLower().Trim() == code.ToLower().Trim());
                if (finded == null && finded.Count() > 1)
                    lastFinded = finded.FirstOrDefault(a => a.name.ToLower().Trim() == regionName.ToLower().Trim());
                else
                if (finded != null && finded.Count() == 1)
                    lastFinded = finded.First();
                if(lastFinded!=null && lastFinded.flag!= null)  
                res = lastFinded.flag;
                return res;

            }catch(Exception ex)
            {
                return ex.InnerException.Message;
            }


        }

        public bool Update(Visitor visitor)
        {
            var updated = false;
            try
            {
                _dataContext.Visitors.Update(visitor);
                _dataContext.SaveChanges();
                updated = true;
            }
            catch(Exception e)
            {
                updated = false;
            }
            return updated;

        }


        public bool GetUserAgreement(string IpAddress)
        {
            var agreed = false;
            try
            {
                var founded = _dataContext.UserAgreements.FirstOrDefault(a => a.IpAddress == IpAddress);
                if (founded != null && founded.Agreed.HasValue && founded.Agreed.Value == true)
                    agreed = true;
                else
                    agreed = false;
            }
            catch (Exception e)
            {
                agreed = false;
            }
            return agreed;

        }

        public UserAgreement AddAgreement(UserAgreement userAgreement)
        {
            var added = _dataContext.UserAgreements.Add(new UserAgreement()
            {
                IpAddress = userAgreement.IpAddress,
                Creationtime = DateTime.Now,
                Agreed = userAgreement.Agreed
            });
            _dataContext.SaveChanges();
            return userAgreement;
        }

        public UserAgreement DeleteAgreement(UserAgreement userAgreement)
        {
            var finded = _dataContext.UserAgreements.FirstOrDefault(a=>a.IpAddress == userAgreement.IpAddress);
            if(finded!=null)
            {
                _dataContext.UserAgreements.Remove(finded);
                _dataContext.SaveChanges();
            }
            
            return userAgreement;
        }

        public void Mute(Guid Id)
        {
            var finded = _dataContext.Visitors.FirstOrDefault(a => a.Id==Id);
            if (finded != null)
            {
                finded.IsMute = true;
                _dataContext.Visitors.Update(finded);
                _dataContext.SaveChanges();
            }
        }

        public void UnMute(Guid Id)
        {
            var finded = _dataContext.Visitors.FirstOrDefault(a => a.Id == Id);
            if (finded != null)
            {
                finded.IsMute = false;
                _dataContext.Visitors.Update(finded);
                _dataContext.SaveChanges();
            }
        }
    }
}
