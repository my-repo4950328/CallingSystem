﻿using WebRTC.Models;
using WebRTC.Models.Dto;

namespace WebRTC.Controllers
{
    public interface IVisitorsAppService
    {
        public VisitorDto CreateVisitor(CreateVisitorDto createVisitorDto);
        public VisitorDto GetByMacAddress(string mac);
        public VisitorDto GetById(Guid id);
        public VisitorDto GetBySessionId(string sessionId);
        public VisitorDto UpdateVisitor(Visitor visitor);
        public Task<bool> LogOutVisitor(Guid Id);
        public Task<bool> LogOutVisitorByConnectionId(string ConnectionId);
        public void LogIVisitor(Guid Id);
        public List<Visitor> GetAll();
        public List<CallOffers> GetAllCallOffers();
        public int GetOnlineCount();

        public CallOffers AddCallOffer(Visitor Caller, Visitor Callee);
        public void DeclineCallOffer(Visitor Caller, Visitor Callee);
        public void AcceptCallOffer(Visitor Caller, Visitor Callee);
        public void HangCall(string ConnectionId);

        public string getCounryFlag(string regionName, string code);
        public bool Update(Visitor visitor);
        public bool GetUserAgreement(string IpAddress);

        public UserAgreement AddAgreement(UserAgreement userAgreement);
        public UserAgreement DeleteAgreement(UserAgreement userAgreement);
        public void Mute(Guid Id);
        public void UnMute(Guid Id);
    }
}
