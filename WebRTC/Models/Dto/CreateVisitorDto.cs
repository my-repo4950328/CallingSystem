﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace WebRTC.Models.Dto
{
    public class CreateVisitorDto
    {
        public string? IpAddress { get; set; } = "";
        public string? MacAddress { get; set; } = "";
        public bool? IsAvailable { get; set; } = false;
        public DateTime? CreationTime { get; set; }
        public DateTime? LastVisitDate { get; set; }
        public DateTime? LastLogOutDate { get; set; }
        public bool InCall { get; set; }
        [DataType("nvarchar(256)")]
        public string? ConnectionId { get; set; }
        public string? SessionId { get; set; }
        public bool? IsMute { get; set; }
    }
}
