﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace WebRTC.Models
{
    public class UserAgreement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [DataType("nvarchar(256)")]
        public string? IpAddress { get; set; } = "";
        public bool? Agreed { get; set; }
        [DataType("datetime2(7)")]
        public DateTime? Creationtime { get; set; } = DateTime.Now;

    }
}
