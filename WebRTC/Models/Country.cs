﻿namespace WebRTC.Models
{
    public class Country
    {
        public Guid Id { get; set;}
        public string? name { get; set;}
        public string? code { get; set; }
        public string? flag { get; set; }
        public DateTime? CreationTime { get; set; }
        public Guid? CreatorId { get; set;}
        public DateTime? LastModificationTime { get; set; }
        public Guid? LastModifierId { get; set; }
        public bool? IsDeleted { get; set; }
        public Guid? DeleterId { get; set; }
        public DateTime? DeletionTime { get; set; } 
    }
}
