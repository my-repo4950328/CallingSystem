﻿using AutoMapper;
using WebRTC.Models.Dto;

namespace WebRTC.Models.MappingProfile
{
    public class VisitorMappingProfile : Profile
    {
        public VisitorMappingProfile()
        {
            CreateMap<Visitor, VisitorDto>().ReverseMap();
            CreateMap<CreateVisitorDto, Visitor>().ReverseMap();
            CreateMap<CreateVisitorDto, VisitorDto>().ReverseMap();
        }
    }
}
