﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace WebRTC.Models
{
    public class CallOffers
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public virtual Visitor? Caller { get; set; }=default(Visitor);
        [ForeignKey(nameof(Visitor))]
        public Guid? CallerId { get; set; }
        public virtual Visitor? Callee { get; set; } = default(Visitor);
        [ForeignKey(nameof(Visitor))]
        public Guid? CalleeId { get; set; }
        [DataType("nvarchar(256)")]
        public string? CallerConnectionId { get; set; }
        [DataType("nvarchar(256)")]
        public string? CallerSessionId { get; set; }
        [DataType("nvarchar(256)")]
        public string? CalleeConnectionId { get; set; }
        [DataType("nvarchar(256)")]
        public string? CalleeSessionId { get; set; }
        public DateTime? CreationTime { get; set; }
        public string? Status { get; set; }
    }
}
