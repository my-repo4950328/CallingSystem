﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WebRTC.Models
{
    public class Visitor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [DataType("nvarchar(256)")]
        public string? IpAddress { get; set; } = "";
        [DataType("nvarchar(256)")]
        public string? MacAddress { get; set; } = "";
        public bool? IsAvailable { get; set; } = false;
        public DateTime? CreationTime { get; set; }
        [Timestamp]
        public byte[] LastModificationTime { get; set; }
        public DateTime? LastVisitDate { get; set; }
        public DateTime? LastLogOutDate { get; set; }
        public bool InCall { get; set; } = false;
        [DataType("nvarchar(256)")]
        public string? ConnectionId { get; set; }
        [DataType("nvarchar(256)")]
        public string? SessionId { get; set; }
        public bool? IsMute { get; set; }
        //public bool? Agreement { get; set; }
    }
}
