﻿using Microsoft.AspNetCore.SignalR;
using System.Net;
using System;
using WebRTC.Controllers;
using WebRTC.Models;
using Newtonsoft.Json.Linq;

namespace WebRTC.Hubs
{
    public class ChatHub : Hub<IConnectionHub>
    {
        private readonly IVisitorsAppService _visitorsAppService;
        //private List<Visitor> _Users;
        private readonly List<UserCall> _UserCalls = new List<UserCall>();
        //private readonly List<CallOffer> _CallOffers;
        private static List<Visitor> _Users = new List<Visitor>();
        //private List<UserCall> _UserCalls = new List<UserCall>();
        private List<CallOffers> _CallOffers = new List<CallOffers>();
        private readonly IHttpContextAccessor _accessor;

        public ChatHub(IVisitorsAppService visitorsAppService, List<UserCall> UserCalls, IHttpContextAccessor accessor)
        {
            _visitorsAppService = visitorsAppService;
            _UserCalls = UserCalls;
            InitUsers();
            _accessor = accessor;
        }


        public override Task OnDisconnectedAsync(Exception? exception)
        {
            _visitorsAppService.LogOutVisitorByConnectionId(Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }

        public override Task OnConnectedAsync()
        {
            var ipAdd = _accessor.HttpContext.Connection.RemoteIpAddress;
            _visitorsAppService.CreateVisitor(new Models.Dto.CreateVisitorDto() { CreationTime = DateTime.Now, IsAvailable = false, LastVisitDate = DateTime.Now, ConnectionId=Context.ConnectionId,IpAddress=ipAdd.ToString(),IsMute=false });
            InitUsers();
            SendUserListUpdate();
            return base.OnConnectedAsync();
        }

        public void InitUsers()
        {
            _Users = _visitorsAppService.GetAll().ToList();
            _CallOffers = _visitorsAppService.GetAllCallOffers().ToList();
        }

        //public async Task SendMessage(string user, string message)
        //{
        //    await Clients.All.SendAsync("ReceiveMessage", user, message);
        //}

        //public async Task Join(string username)
        //{
        //    // Add the new user
        //    InitUsers();
        //    if (_Users!=null && _Users.Count()>0)
        //    {
        //        foreach(var user in _Users)
        //        {
        //            if (user.Id == Guid.Parse(username))
        //            {
        //                user.ConnectionId = Context.ConnectionId;
        //                user.IsAvailable = true;
        //                _visitorsAppService.UpdateVisitor(user);
        //            }
                        
        //        }
        //        InitUsers();
        //    }
        //    // Send down the new list to all clients
        //    await SendUserListUpdate();
        //}

        public async Task UpdateUsers()
        {
            InitUsers();
            await SendUserListUpdate();
        }

        //public async Task CallUser()
        //{
        //    var callingUser = _Users.SingleOrDefault(u => u.ConnectionId == Context.ConnectionId);
        //    if (_Users != null && _Users.Count() > 1)
        //    {
        //        var targetUser = _Users.SingleOrDefault(u => u.ConnectionId != Context.ConnectionId && u.IsAvailable.HasValue && u.IsAvailable.Value && u.InCall==true);

        //        //    // Make sure the person we are trying to call is still here
        //        //    if (targetUser == null)
        //        //    {
        //        //        // If not, let the caller know
        //        //        await Clients.Caller.CallDeclined(targetConnectionId, "The user you called has left.");
        //        //        return;
        //        //    }

        //        //    // And that they aren't already in a call
        //        //    if (GetUserCall(targetUser.ConnectionId) != null)
        //        //    {
        //        //        await Clients.Caller.CallDeclined(targetConnectionId, string.Format("{0} is already in a call.", targetUser.Username));
        //        //        return;
        //        //    }

        //        // They are here, so tell them someone wants to talk
        //        await Clients.Client(targetUser.ConnectionId).IncomingCall(callingUser);

        //        // Create an offer
        //        _CallOffers.Add(new CallOffer
        //        {
        //            Caller = callingUser,
        //            Callee = targetUser
        //        });
        //    }
        //    else
        //    {
        //        await Clients.Caller.NoUsersToCall("No online users to call");
        //    }
        //}
       
        public async Task rejectAgreement()
        {
            var callingUser = _Users.SingleOrDefault(u => u.ConnectionId == Context.ConnectionId);
            if (callingUser != null)
            {
                _visitorsAppService.DeleteAgreement(new UserAgreement()
                {
                    IpAddress = callingUser.IpAddress
                });
            }
            await Clients.Client(callingUser.ConnectionId).Reject();
        }
        
        public async Task acceptAgreement()
        {
            var callingUser = _Users.SingleOrDefault(u => u.ConnectionId == Context.ConnectionId);
            if (callingUser != null)
            {
                _visitorsAppService.AddAgreement(new UserAgreement() { 
                    Agreed=true,
                    IpAddress = callingUser.IpAddress
                });
            }
            await CallUser();
        }
        public async Task MakeInvisable()
        {
            var callingUser = _Users.SingleOrDefault(u => u.ConnectionId == Context.ConnectionId);
            if (callingUser != null)
            {
                callingUser.IsAvailable = false;
                var updated = _visitorsAppService.Update(callingUser);
            }
        }
        
        public async Task CallUser()
        {
            var callingUser = _Users.SingleOrDefault(u => u.ConnectionId == Context.ConnectionId);
            if(callingUser!=null)
            {
                var checkAgreement = _visitorsAppService.GetUserAgreement(callingUser.IpAddress);
                if(checkAgreement)
                {
                    await Clients.Client(callingUser.ConnectionId).Agree();
                    callingUser.IsAvailable = true;
                    var updated = _visitorsAppService.Update(callingUser);
                    if (_Users != null && _Users.Where(a => a.IsAvailable.HasValue && a.IsAvailable.Value == true).Count() > 0 && callingUser != null)
                    {
                        Visitor? targetUser = null;
                        var availableUsers = _Users.Where(u => u.IsAvailable.HasValue && u.IsAvailable.Value && u.InCall == false && u.ConnectionId != callingUser.ConnectionId).ToList();
                        if (availableUsers != null && availableUsers.Count() > 0)
                        {
                            Random random = new Random();
                            var ind = random.Next(availableUsers.Count());
                            targetUser = availableUsers[ind];
                        }
                        if (targetUser != null && targetUser.ConnectionId != null && GetUserCall(targetUser.ConnectionId) == null)
                        {
                            _visitorsAppService.AddCallOffer(callingUser, targetUser);
                            if (targetUser != null && targetUser.ConnectionId != null)
                                await Clients.Client(targetUser.ConnectionId).IncomingCall(callingUser);
                        }
                    }
                }
                else
                {
                    await Clients.Client(callingUser.ConnectionId).UserAgreement(callingUser);
                }

            }
            //if (_Users != null && _Users.Count() > 0 && callingUser!=null)
            //{
            //    Visitor? targetUser = null;
            //    var availableUsers = _Users.Where(u => u.IsAvailable.HasValue && u.IsAvailable.Value && u.InCall == false && u.ConnectionId!=callingUser.ConnectionId).ToList();
            //    if(availableUsers!=null && availableUsers.Count()>0)
            //    {
            //        Random random = new Random();
            //        var ind = random.Next(availableUsers.Count());
            //        targetUser = availableUsers[ind];
            //    }

            //    // Make sure the person we are trying to call is still here
            //    if (targetUser == null || (targetUser!=null && targetUser.ConnectionId==null))
            //    {
            //        // If not, let the caller know
            //        await Clients.Caller.CallDeclined(callingUser,"no online user to call.");
            //        return;
            //    }

            //    //    // And that they aren't already in a call
            //    //    if (GetUserCall(targetUser.ConnectionId) != null)
            //    //    {
            //    //        await Clients.Caller.CallDeclined(targetConnectionId, string.Format("{0} is already in a call.", targetUser.Username));
            //    //        return;
            //    //    }

            //    // Create an offer
            //    _visitorsAppService.AddCallOffer(callingUser, targetUser);

            //    // They are here, so tell them someone wants to talk
            //    if (targetUser!=null && targetUser.ConnectionId!=null)
            //        await Clients.Client(targetUser.ConnectionId).IncomingCall(callingUser);


            //}
            //else
            //{
            //    await Clients.Caller.NoUsersToCall("No online users to call");
            //}
        }

        private UserCall GetUserCall(string connectionId)
        {
            var matchingCall =
                _UserCalls.SingleOrDefault(uc => uc.Users.SingleOrDefault(u => u.ConnectionId == connectionId) != null);
            return matchingCall;
        }

        private async Task SendUserListUpdate()
        {
            //_Users.ForEach(u => u.InCall = (GetUserCall(u.ConnectionId) != null));
            //await CallUser();
            await Clients.All.UpdateUserList(_Users);
        }

        public async Task AnswerCall(bool acceptCall, Visitor targetConnectionId)
        {
            var callingUser = _Users.SingleOrDefault(u => u.ConnectionId == Context.ConnectionId);
            var targetUser = _Users.SingleOrDefault(u => u.ConnectionId == targetConnectionId.ConnectionId);

            // This can only happen if the server-side came down and clients were cleared, while the user
            // still held their browser session.
            if (callingUser == null)
            {
                return;
            }

            // Make sure the original caller has not left the page yet
            if (targetUser == null)
            {
                await Clients.Caller.CallEnded(targetConnectionId, "The other user in your call has left.");
                return;
            }

            // Send a decline message if the callee said no
            if (acceptCall == false)
            {
                _visitorsAppService.DeclineCallOffer(targetUser,callingUser);
                await Clients.Client(targetConnectionId.ConnectionId).CallDeclined(callingUser, string.Format("User did not accept your call."));
                return;
            }

            // Make sure there is still an active offer.  If there isn't, then the other use hung up before the Callee answered.
            InitUsers();
            //var offerCount = _CallOffers.RemoveAll(c => c.Callee.ConnectionId == callingUser.ConnectionId
            //                                      && c.Caller.ConnectionId == targetUser.ConnectionId);
            //if (offerCount < 1)
            //{
            //    await Clients.Caller.CallEnded(targetConnectionId, string.Format("User has already hung up."));
            //    return;
            //}

            // And finally... make sure the user hasn't accepted another call already
            //if (GetUserCall(targetUser.ConnectionId) != null)
            //{
            //    // And that they aren't already in a call
            //    await Clients.Caller.CallDeclined(targetConnectionId, string.Format("chose to accept someone elses call instead of yours :("));
            //    return;
            //}

            //// Remove all the other offers for the call initiator, in case they have multiple calls out
            ////_CallOffers.RemoveAll(c => c.Caller.ConnectionId == targetUser.ConnectionId);

            //// Create a new call to match these folks up
            _UserCalls.Add(new UserCall
            {
                Users = new List<Visitor> { callingUser, targetUser }
            });

            callingUser.IsAvailable = false;
            targetUser.IsAvailable = false;
            // Tell the original caller that the call was accepted
            _visitorsAppService.AcceptCallOffer(targetUser, callingUser);
            await Clients.Client(targetConnectionId.ConnectionId).CallAccepted(callingUser);

            //// Update the user list, since thes two are now in a call
            //await SendUserListUpdate();
        }

        public async Task Mute()
        {
            var callingUser = _Users.SingleOrDefault(u => u.ConnectionId == Context.ConnectionId);

            if (callingUser == null)
            {
                return;
            }
            _visitorsAppService.Mute(callingUser.Id);
            var currentCall = GetUserCall(callingUser.ConnectionId);
            if(currentCall!=null && currentCall.Users.FirstOrDefault(a=>a.Id!=callingUser.Id)!=null)
            {
                var otherSide = currentCall.Users.FirstOrDefault(a => a.Id != callingUser.Id);
                await Clients.Client(otherSide.ConnectionId).OtherSideMute();
            }
        }

        public async Task UnMute()
        {
            var callingUser = _Users.SingleOrDefault(u => u.ConnectionId == Context.ConnectionId);

            if (callingUser == null)
            {
                return;
            }
            _visitorsAppService.UnMute(callingUser.Id);
            var currentCall = GetUserCall(callingUser.ConnectionId);
            if (currentCall != null && currentCall.Users.FirstOrDefault(a => a.Id != callingUser.Id) != null)
            {
                var otherSide = currentCall.Users.FirstOrDefault(a => a.Id != callingUser.Id);
                await Clients.Client(otherSide.ConnectionId).OtherSideUnMute();
            }
        }

        public async Task HangUp()
        {
            var callingUser = _Users.SingleOrDefault(u => u.ConnectionId == Context.ConnectionId);

            if (callingUser == null)
            {
                return;
            }

            var currentCall = GetUserCall(callingUser.ConnectionId);

            // Send a hang up message to each user in the call, if there is one
            if (currentCall != null)
            {
                foreach (var user in currentCall.Users)
                {
                    _visitorsAppService.HangCall(user.ConnectionId);
                    //if(user.ConnectionId!=callingUser.ConnectionId)
                        await Clients.Client(user.ConnectionId).CallEnded(callingUser, string.Format("user has hung up. maybe to mars."));
                    
                }
                //foreach (var user in currentCall.Users.Where(u => u.ConnectionId != callingUser.ConnectionId))
                //{
                //    await Clients.Client(user.ConnectionId).CallEnded(callingUser, string.Format("user has hung up."));
                //    _visitorsAppService.HangCall(user.ConnectionId);
                //}

                // Remove the call from the list if there is only one (or none) person left.  This should
                // always trigger now, but will be useful when we implement conferencing.
                currentCall.Users.RemoveAll(u => u.ConnectionId == callingUser.ConnectionId);
                if (currentCall.Users.Count < 2)
                {
                    _UserCalls.Remove(currentCall);
                }
            }

            // Remove all offers initiating from the caller
            _CallOffers.RemoveAll(c => c.Caller.ConnectionId == callingUser.ConnectionId);

            await SendUserListUpdate();
        }

        // WebRTC Signal Handler
        public async Task SendSignal(string signal, string targetConnectionId)
        {
            var callingUser = _Users.SingleOrDefault(u => u.ConnectionId == Context.ConnectionId);
            var targetUser = _Users.SingleOrDefault(u => u.ConnectionId == targetConnectionId);

            // Make sure both users are valid
            if (callingUser == null || targetUser == null)
            {
                return;
            }
            string regionName = string.Empty;
            string code = string.Empty;
            string html = string.Empty;
            string flag = string.Empty;

            if (callingUser != null && callingUser.IpAddress!=null && callingUser.IpAddress!="::1")
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://pro.ip-api.com/json/" + callingUser.IpAddress + "?key=7nAIZw6o3cSv9AK");
                if (request != null)
                {
                    request.AutomaticDecompression = DecompressionMethods.GZip;
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                    regionName = JObject.Parse(html)["regionName"].ToString();
                    code = JObject.Parse(html)["countryCode"].ToString();
                }

                flag = _visitorsAppService.getCounryFlag(regionName,code);
            }

            //var flag = _visitorsAppService.getCounryFlag("Jordan");
            await Clients.Client(targetConnectionId).ReceiveSignal(callingUser, signal,flag);

            //// Make sure that the person sending the signal is in a call
            //var userCall = GetUserCall(callingUser.ConnectionId);

            //// ...and that the target is the one they are in a call with
            //if (userCall != null && userCall.Users.Exists(u => u.ConnectionId == targetUser.ConnectionId))
            //{
            //    // These folks are in a call together, let's let em talk WebRTC
            //    await Clients.Client(targetConnectionId).ReceiveSignal(callingUser, signal);
            //}
        }

    }




    public interface IConnectionHub
    {
        Task UpdateUserList(List<Visitor> userList);
        Task CallAccepted(Visitor acceptingUser);
        Task NoUsersToCall(string reason);
        Task CallDeclined(Visitor decliningUser, string reason);
        Task IncomingCall(Visitor callingUser);
        Task ReceiveSignal(Visitor signalingUser, string signal,string flag);
        Task CallEnded(Visitor signalingUser, string signal);
        Task UserAgreement(Visitor callingUser);
        Task Agree();
        Task Reject();
        Task OtherSideMute();
        Task OtherSideUnMute();
    }
}
