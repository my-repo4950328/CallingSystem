﻿using Microsoft.EntityFrameworkCore;
using WebRTC.Models;

namespace WebRTC.Helpers
{
    public class DataContext : DbContext
    {
        protected readonly IConfiguration Configuration;

        public DataContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to sql server with connection string from app settings
            options.UseSqlServer(Configuration.GetConnectionString("WebRTCDatabase"));
        }

        public DbSet<Visitor> Visitors { get; set; }
        public DbSet<CallOffers> CallOffers { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<UserAgreement> UserAgreements { get; set; }    
    }
}
